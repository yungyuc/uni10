#ifndef __UNI10_LINALG_API_H__
#define __UNI10_LINALG_API_H__

#include "uni10_api/uni10_linalg/uni10_linalg_getdiag.h"
#include "uni10_api/uni10_linalg/uni10_linalg_dot.h"
#include "uni10_api/uni10_linalg/uni10_linalg_qr.h"
#include "uni10_api/uni10_linalg/uni10_linalg_rq.h"
#include "uni10_api/uni10_linalg/uni10_linalg_lq.h"
#include "uni10_api/uni10_linalg/uni10_linalg_ql.h"
#include "uni10_api/uni10_linalg/uni10_linalg_qdr.h"
#include "uni10_api/uni10_linalg/uni10_linalg_ldq.h"
#include "uni10_api/uni10_linalg/uni10_linalg_qdr_cpivot.h"
#include "uni10_api/uni10_linalg/uni10_linalg_svd.h"
#include "uni10_api/uni10_linalg/uni10_linalg_sdd.h"
#include "uni10_api/uni10_linalg/uni10_linalg_eigh.h"
#include "uni10_api/uni10_linalg/uni10_linalg_eig.h"
#include "uni10_api/uni10_linalg/uni10_linalg_inverse.h"
#include "uni10_api/uni10_linalg/uni10_linalg_sum.h"
#include "uni10_api/uni10_linalg/uni10_linalg_norm.h"
#include "uni10_api/uni10_linalg/uni10_linalg_trace.h"
#include "uni10_api/uni10_linalg/uni10_linalg_transpose.h"
#include "uni10_api/uni10_linalg/uni10_linalg_dagger.h"
#include "uni10_api/uni10_linalg/uni10_linalg_conj.h"
#include "uni10_api/uni10_linalg/uni10_linalg_det.h"
#include "uni10_api/uni10_linalg/uni10_linalg_exph.h"

#endif
