#ifndef __TEST_OPTS_H__
#define __TEST_OPTS_H__

#include "uni10.hpp"

#include <string>
#include <boost/program_options.hpp>

class Opts {
  public:
    // Constructor
    Opts();

    int parse(int argc, char **argv);

    unsigned int Rnum();

    unsigned int Cnum();

    unsigned int ntest();

    bool inplace();

  private:
    unsigned int _Rnum;
    unsigned int _Cnum;
    unsigned int _ntest;
    bool         _inplace;

    boost::program_options::options_description _desc;
    boost::program_options::variables_map _vmap;
};

#endif
