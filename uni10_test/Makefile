UNI10_ROOT=/usr/local/uni10
### BLAS configuration ###
ifneq ($(shell readelf --dynamic $(UNI10_ROOT)/lib/libuni10*.so | grep atlas),)
	CXX := g++
	BLASFLAGS := -lblas -llapack -latlas
else ifneq ($(shell readelf --dynamic $(UNI10_ROOT)/lib/libuni10*.so | grep mkl),)
	CXX := icpc
	BLASFLAGS := -lmkl_rt
endif

### Set directories ###
SRC_DIR := ./src
BUILD_DIR := ./build
INC_DIRS := $(UNI10_ROOT)/include ./include
LIB_DIRS := $(UNI10_ROOT)/lib

### Compiler and Linker configuration ###
LIB := $(foreach dir,$(LIB_DIRS),-L$(dir)) $(BLASFLAGS) -luni10_lapack_cpu -lpthread -lboost_program_options
INC := $(foreach dir,$(INC_DIRS),-I$(dir))
CXXFLAGS := $(INC) -m64 -std=c++11 -O2 -pipe
LDFLAGS := $(LIB) $(INC) -O2

### Testing tools ###
LINALG_TOOLS := linalg/test_dsdd linalg/test_zsdd \
				linalg/test_dsvd linalg/test_zsvd \
				linalg/test_dqr linalg/test_zqr \
				linalg/test_dql linalg/test_zql \
				linalg/test_drq linalg/test_zrq \
				linalg/test_dlq linalg/test_zlq \
				linalg/test_dqdr linalg/test_zqdr \
				linalg/test_dldq linalg/test_zldq \
				linalg/test_dqdrcpivot linalg/test_zqdrcpivot \

TOOLS := $(LINALG_TOOLS)

### Targets ###
all: $(TOOLS)

$(TOOLS): %: $(BUILD_DIR)/%.o $(BUILD_DIR)/test_tool/test_perf.o \
		$(BUILD_DIR)/test_tool/test_systool.o \
		$(BUILD_DIR)/test_tool/test_opts.o
	@mkdir -p $(BUILD_DIR)
	$(CXX) $(LDFLAGS) $^ -o $(BUILD_DIR)/$@

$(BUILD_DIR)/linalg/%.o: $(SRC_DIR)/linalg/%.cpp
	@mkdir -p $(BUILD_DIR)/linalg
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(BUILD_DIR)/test_tool/%.o: $(SRC_DIR)/test_tool/%.cpp
	@mkdir -p $(BUILD_DIR)/test_tool
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -rf $(BUILD_DIR)
