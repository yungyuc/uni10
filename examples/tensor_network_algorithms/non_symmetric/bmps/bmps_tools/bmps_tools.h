#ifndef __BMPS_TOOLS_H__
#define __BMPS_TOOLS_H__

#include "uni10.hpp"

using namespace std;
using namespace uni10;

template<typename T>
void bondcat(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx);

template<typename T>
void bondsqrtcat(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx);

template<typename T>
UniTensor<T> contractSelf( UniTensor<T> &tin, const vector<int> &posits);

template<typename T>
void combineTwoLayer( UniTensor<T> &tensor );

template<typename T>
void bondrm(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx);

template<typename T>
void bonsqrtdrm(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx);

template<typename T>
void bondcat(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx){

  uni10_int32 InBondNum = Tout.InBondNum();
  vector<uni10_int32> labels = Tout.label();
  vector<uni10_int32> per_labels = labels;
  uni10_int32 l = labels[bidx];
  per_labels.erase(per_labels.begin() + bidx);
  per_labels.insert(per_labels.begin(), l);

  UniTensor<T> T_c = Permute(Tout, per_labels, 1);
  T_c.PutBlock((Dot(L, T_c.GetBlock())));
  Tout = Permute( T_c, labels, InBondNum);
}

template<typename T>
void bondsqrtcat(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx){
  Matrix<T> sqrtL = L;
  for(uni10_uint64 i=0; i!=L.col(); i++){
    sqrtL[i] = sqrt( sqrtL[i] );
  }
  bondcat( Tout, sqrtL, bidx );
}

template<typename T>
UniTensor<T> contractSelf( UniTensor<T> &tin, const vector<int> &posits){
  vector<int> tcopyLab = tin.label();
  int max=*max_element( tcopyLab.begin(), tcopyLab.end());
  for ( int i=0; i!=tcopyLab.size(); i++){
    tcopyLab.at(i) = max+i+1;
  }
  for ( int i=0; i!=posits.size(); i++){
    tcopyLab.at( posits.at(i) ) = tin.label().at( posits.at(i) );
  }
  UniTensor<T> tcopy = Dagger( tin );
  tcopy = Transpose( tcopy );
  //UniTensor<T> tcopy = tin;
  tcopy.SetLabel( tcopyLab );
  UniTensor<T> tout;
  tout = Contract( tin, tcopy, false );
  return tout;
}

template<typename T>
void combineTwoLayer( UniTensor<T> &tensor ){
  const int bdN = tensor.BondNum();
  assert( bdN%2==0 );
  int newbdN = bdN/2;
  vector<int> oldLab = tensor.label();
  vector<int> combineLab(2);
  for (int i=0; i!=newbdN; i++){
    combineLab.at(0) = oldLab.at(i);
    combineLab.at(1) = oldLab.at(newbdN+i);
    tensor.CombineBond( combineLab );
  }
}

template<typename T>
void bondrm(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx){

  Matrix<T> invL = L;
  for(uni10_uint64 i=0; i!=L.col(); i++){
    invL[i] = invL[i] == 0.0 ? 0.0 : ( 1.0 / invL[i]);
  }
  bondcat(Tout, invL, bidx);

}

template<typename T>
void bondsqrtrm(UniTensor<T>& Tout, const Matrix<T>& L, uni10_int32 bidx){

  Matrix<T> invL = L;
  for(uni10_uint64 i=0; i!=L.col(); i++){
    invL[i] = invL[i] == 0.0 ? 0.0 : ( 1.0 / sqrt(invL[i]) );
  }
  bondcat(Tout, invL, bidx);
}

#endif
