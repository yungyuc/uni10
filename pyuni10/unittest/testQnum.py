## Fermionic is set once PRTF_ODD is assigned.

import pyuni10 as uni10
import unittest

class TestQnum( unittest.TestCase ):

    def testDeafaultConstructor( self ):
         q = uni10.Qnum()
         self.assertEqual( 0, q.U1() )
         self.assertEqual( uni10.PRT_EVEN, q.prt() )
         ## self.assertFalse( q.isFermionic() )

    def testDefaultFermionConstructor( self ):
        q = uni10.Qnum( prtF=uni10.PRTF_ODD )
        self.assertEqual( uni10.PRTF_ODD, q.prtF() )
        self.assertEqual( 0, q.U1() )
        self.assertTrue( q.isFermionic() )

    def testCopyConstructor( self ):
        q1 = uni10.Qnum()
        q2 = uni10.Qnum( q1 )
        self.assertEqual( q1, q2 )

    def testIsFermionic( self ):
        q = uni10.Qnum()
        self.assertEqual( 0, q.U1() )
        self.assertEqual( uni10.PRT_EVEN, q.prt() )
        ## PRTF_ODD is defined, so fermionic is set.
        self.assertTrue( q.isFermionic() )
    
    def testAssign( self ):
        q = uni10.Qnum()
        q.assign( U1=3 )
        self.assertEqual( 3, q.U1() )
        self.assertEqual( uni10.PRT_EVEN, q.prt() )
        
        q.assign( U1=0, prt=uni10.PRT_ODD )
        self.assertEqual( 0, q.U1() )
        self.assertEqual( uni10.PRT_ODD, q.prt() )

        q.assign( U1=-1 )
        self.assertEqual( -1, q.U1() )
        self.assertEqual( uni10.PRT_EVEN, q.prt() ) ## Default value prt = PRT_EVEN.
        
        qf = uni10.Qnum()
        qf.assign( prtF=uni10.PRTF_EVEN, U1=1, prt=uni10.PRT_ODD )
        self.assertEqual( uni10.PRTF_EVEN, qf.prtF() )
        self.assertEqual( 1, qf.U1() )
        self.assertEqual( uni10.PRT_ODD, qf.prt() )

        qf.assign( prtF=uni10.PRTF_ODD )
        self.assertEqual( uni10.PRTF_ODD, qf.prtF() )
        self.assertEqual( 0, qf.U1() ) ## Default value U1 = 0.
        self.assertEqual( uni10.PRT_EVEN, qf.prt() ) ## Default value prt = PRT_EVEN.

    def testHash( self ):
        q = uni10.Qnum( prtF=uni10.PRTF_ODD, U1=100, prt=uni10.PRT_EVEN )
        self.assertEqual( 401,  hash(q) )

    def testOperationNegation( self ):
        q1 = uni10.Qnum( -1 )
        q1 = -q1
        self.assertEqual( 1, q1.U1() )
        self.assertEqual( uni10.PRT_EVEN, q1.prt() )

        q2 = uni10.Qnum( prtF=uni10.PRTF_EVEN, U1=3, prt=uni10.PRT_ODD)
        q2 = -q2
        self.assertEqual( -3, q2.U1() )
        self.assertEqual( uni10.PRT_ODD, q2.prt() )
        self.assertEqual( uni10.PRTF_EVEN, q2.prtF() )

    def testOperationLessThan( self ):
        q1 = uni10.Qnum( 3 )
        q2 = uni10.Qnum( -1 )
        
        self.assertTrue( q2 < q1 )
        q1.assign( prtF=uni10.PRTF_ODD )
        q2.assign( prtF=uni10.PRTF_EVEN )

        self.assertTrue( q2 < q1 )
        q1.assign( prt=uni10.PRT_ODD )
        q2.assign( prt=uni10.PRT_EVEN )

        self.assertTrue( q2 < q1 )

    def testOeprationLessEq( self ):
        q1 = uni10.Qnum( 3 )
        q2 = uni10.Qnum( -1 )

        self.assertTrue( q2 <= q1 )
        q1.assign( -1 )
        self.assertTrue( q2 <= q1 )

    def testOperationEq( self ):
        q1 = uni10.Qnum( 3 )
        q2 = uni10.Qnum( 2 )

        self.assertFalse( q2 == q1 )

        q1.assign( U1=0, prt=uni10.PRT_ODD )
        q2.assign( U1=0, prt=uni10.PRT_EVEN )
        self.assertFalse( q2 == q1 )

        q1.assign( prtF=uni10.PRTF_EVEN, U1=0, prt=uni10.PRT_EVEN )
        q2.assign( prtF=uni10.PRTF_ODD, U1=0, prt=uni10.PRT_EVEN )
        self.assertFalse( q2 == q1 )

    def testOperationMul( self ):
        q1 = uni10.Qnum( prtF=uni10.PRTF_EVEN, U1=3, prt=uni10.PRT_ODD )
        q2 = uni10.Qnum( prtF=uni10.PRTF_ODD, U1=2, prt=uni10.PRT_ODD )
        q3 = q1 * q2
        
        self.assertEqual( 5, q3.U1() )
        self.assertEqual( uni10.PRT_EVEN, q3.prt() )
        self.assertEqual( uni10.PRTF_ODD, q3.prtF() )

        q4 = uni10.Qnum( U1=-1, prt=uni10.PRT_ODD )
        q5 = uni10.Qnum( U1=2, prt=uni10.PRT_ODD )
        q3 = q4 * q5

        self.assertEqual( 1, q3.U1() )
        self.assertEqual( uni10.PRT_EVEN, q3.prt() )
        self.assertNotEqual( uni10.PRTF_ODD, q3.prtF() )


if __name__ == "__main__":
    unittest.main()
