1. Cannot wrap template class to one Python class.

Ex: 
    ========= NO ==========
    Block<REAL>    -> Block
    Block<COMPLEX> -> Block

    ========= YES =========
    Block<REAL>    -> BlockR
    Block<COMPLEX> -> BlockC


2. cannot find -lmsvcr140

Go to Python_dir/Lib/distutils, open "cygwinccompiler.py".
Find "msvcr140", which is in the return value of function "get_msvcr".
Replace "msvcr140" with "vcruntime140".


3. Unknown MS compiler version 1900

Go to Python_dir/Lib/distutils, open "cygwinccompiler.py".
Find function "get_msvcr".
Add the following block into the function. (Right after "return ['msvcr100']")

elif int(msc_ver) >= 1900:
	#VS2015 / MSVC 14.0
	return ['vcruntime140']


4. cannot find -lvcruntime140

Go to Python_dir, find file "vcruntime140.dll".
Copy the file to Python_dir/libs, then open terminal.
Type the following command.(May have to download gendef and put it to MinGW/bin)

gendef vcruntime140.dll
dlltool --dllname vcruntime140.dll --def vcruntime140.def --output-lib libvcruntime140.a


5. undefined reference to '_imp__Py_*'

Go to Python_dir, find file "python**.dll".(Replace stars to python version.)
Copy the file to Python_dir/libs, then open terminal.
Type the following command.(May have to download gendef and put it to MinGW/bin)

gendef python**.dll
dlltool --dllname python**.dll --def python**.def --output-lib libpython**.a


6. Cannot wrap template functions with return values.

Use lambda function instead.


7. Add get_status_enforce() & get_U_elem_enforce() in UniTensor.h


8. Need to include "uni10_linalg_eigh.h" in "uni10_linalg_exph.h"


9. Undefined reference to UniTensor<double> - UniTensor<complex>


10. Passing "const uni10::Block<UniType>&" as "this" in ...

In "uni10_hirnk_linalg_trace.h", line 27
Replace "it->second.row_enforce() == it->second.col_enforce()" by "it->second.row() == it->second.col()"


11. Undefined reference to "void PartialTrace"


12. Use py::array_t<> to force numpy array as input
    py::array_t<uni10_double64, py::array::c_style | py::array::forcecast>
    enforce c-sytle matrix and perform type cast 
